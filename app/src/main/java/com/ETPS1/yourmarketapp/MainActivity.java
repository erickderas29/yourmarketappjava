package com.ETPS1.yourmarketapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ETPS1.yourmarketapp.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    Button btnlog1;
    TextView tving1;
    EditText etrtel1,etrpass1;
    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         btnlog1 = findViewById(R.id.btnreg1);
         tving1 = findViewById(R.id.tving1);
         etrtel1 = findViewById(R.id.etrtel1);
         etrpass1 = findViewById(R.id.etrpass1);

        //Iniciamos Firebase
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("User");

        btnlog1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProgressDialog dialog = new ProgressDialog(MainActivity.this);
                dialog.setMessage("Procesando...");
                dialog.show();

                table_user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        if (snapshot.child(etrtel1.getText().toString()).exists()) {


                            dialog.dismiss();
                            User user = snapshot.child(etrtel1.getText().toString()).getValue(User.class);
                            if (user.getPassword().equals(etrpass1.getText().toString())) {
                                Toast.makeText(MainActivity.this, "Bienvenido a YourMarket", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            dialog.dismiss();
                            Toast.makeText(MainActivity.this,"El usuario no existe", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });

        tving1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, user_register.class);
                startActivity(intent);
            }
        });
    }
}