package com.ETPS1.yourmarketapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ETPS1.yourmarketapp.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class user_register extends AppCompatActivity {

    EditText etrname1,etrtel1,etrpass1;
    Button btnreg1;
    TextView tving1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_register);

        etrname1 = findViewById(R.id.etrname1);
        etrtel1 = findViewById(R.id.etrtel1);
        etrpass1 = findViewById(R.id.etrpass1);
        btnreg1 = findViewById(R.id.btnreg1);
        tving1 = findViewById(R.id.tving1);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("User");

        btnreg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog dialog = new ProgressDialog(user_register.this);
                dialog.setMessage("Procesando...");
                dialog.show();

                table_user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.child(etrtel1.getText().toString()).exists()){
                            dialog.dismiss();
                            Toast.makeText(user_register.this,"El telefono ya existe",Toast.LENGTH_SHORT).show();
                        }else {
                            dialog.dismiss();
                            User user = new User(etrname1.getText().toString(),etrpass1.getText().toString());
                            table_user.child(etrtel1.getText().toString()).setValue(user);
                            Toast.makeText(user_register.this,"Usuario registrado con exito",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });

        tving1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(user_register.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}